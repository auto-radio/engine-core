-include build/base.Makefile
-include build/docker.Makefile
-include build/audio.Makefile


help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        	- init application environment"
	@echo "    init.dev        	- init development environment"
	@echo "    lint            	- verify code style"
	@echo "    spell           	- check spelling of text"
	@echo "    test            	- run test suite"
	@echo "    log             	- tail log file"
	@echo "    tns             	- connect to telnet server"
	@echo "    run             	- start app"
	@echo "    run.debug       	- start app in debug mode"
	@echo "    release         	- tag and push release with current version"
	@echo "    docker.init     	- create docker volume for engine socket"
	$(call docker_help)
	$(call audio_help)
	@echo "    audio.lqs.test  	- test audio device using liquidsoap"


# Settings

AURA_ENGINE_CORE_ALSA_CONFIG := ${CURDIR}/config/asound.conf
AURA_ENGINE_CORE_SOCKET := "aura_engine_socket"
AURA_ENGINE_CORE_CONFIG := ${CURDIR}/config/engine-core.docker.ini
AURA_AUDIO_STORE_SOURCE := ${CURDIR}/audio/source
AURA_AUDIO_STORE_PLAYLIST := ${CURDIR}/audio/playlist
AURA_AUDIO_STORE_FALLBACK := ${CURDIR}/audio/fallback
AURA_LOGS := ${CURDIR}/logs
AURA_UID := 2872
AURA_GID := 2872

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		--mount type=tmpfs,destination=/tmp \
		--device /dev/snd \
		--group-add audio \
		--ulimit rtprio=95 --ulimit memlock=-1 --shm-size=512m \
		--env-file docker.env \
		-v "$(AURA_ENGINE_CORE_ALSA_CONFIG)":"/etc/asound.conf" \
		-v "$(AURA_ENGINE_CORE_SOCKET)":"/srv/socket" \
		-v "$(AURA_ENGINE_CORE_CONFIG)":"/etc/aura/engine-core.ini":ro \
		-v "$(AURA_AUDIO_STORE_SOURCE)":"/var/audio/source":ro \
		-v "$(AURA_AUDIO_STORE_PLAYLIST)":"/var/audio/playlist":ro \
		-v "$(AURA_AUDIO_STORE_FALLBACK)":"/var/audio/fallback":ro \
		-v "$(AURA_LOGS)":"/srv/logs" \
		-p 1234:1234 \
		-u $(AURA_UID):$(AURA_GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)

# Targets

lint::
	liquidsoap --check src/engine.liq

spell::
	codespell $(wildcard *.md) docs src tests config contrib

init.app::
	cp -n config/sample.engine-core.ini config/engine-core.ini || true
	mkdir -p audio/fallback
	mkdir -p audio/playlist
	mkdir -p audio/source

init.dev:: init.app
	sudo apt install -y codespell
	sudo apt install -y pre-commit
	pre-commit autoupdate
	pre-commit install

test::
	(cd tests && liquidsoap engine_test_suite.liq)

log::
	tail -f logs/$(APP_NAME).log

tns::
	telnet 0.0.0.0 1234

run::
	(cd src && liquidsoap ./engine.liq)

run.debug::
	(cd src && liquidsoap --verbose --debug ./engine.liq)

docker.init::
	@echo "Creating Docker Volume for Engine Socket at '$(AURA_ENGINE_CORE_SOCKET)'"
	docker volume create aura_engine_socket

audio.lqs.test::
	liquidsoap tests/device/alsa_device_aura_engine.liq

release:: VERSION = $$(cat ${CURDIR}/VERSION)
release::
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."