# AURA Engine Core

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/engine-core/-/badges/release.svg)](https://gitlab.servus.at/aura/engine-core/-/releases) [![pipeline status](https://gitlab.servus.at/aura/engine-core/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/engine-core/-/commits/main)

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="120" align="right" />

Engine Core is a multi-channel playout server for radio stations based on [Liquidsoap](https://www.liquidsoap.info/).
It is able to broadcast via audio device outputs or connect to Icecast streams.

This documentation is primarily meant for developers. For using the AURA Community Radio Suite
check out the documentation at [docs.aura.radio](https://docs.aura.radio/)

To learn more about Automated Radio go to [aura.radio](https://aura.radio).

## Channel Routing

Playout channels are routed this way:

```mermaid
graph TD
    iq0[Queue A] -->|in_queue_0| mix
    iq1[Queue B] -->|in_queue_1| mix
    is0[Stream A] -->|in_stream_0| mix
    is1[Stream B] -->|in_stream_1| mix
    il0[Line In 1-5] -->|in_line_0..4| mix
    ff[Fallback Folder] -->|fallback_folder| which_fallback
    fpls[Fallback Playlist] -->|fallback_playlist| which_fallback
    mix["  Mixer  "] --> silence_detector
    which_fallback{or} -->| | silence_detector{Silence Detector}
    silence_detector -->| | output[Output]
    output --> |output.alsa| C[fa:fa-play Audio Interface]
    output --> |output.icecast| D[fa:fa-play Icecast]
```

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Operating system: Debian 11, Ubuntu 20.04 or newer
- Audio device which has good [ALSA](https://alsa-project.org/wiki/Main_Page) support
- [Liquidsoap 2.1.x](https://www.liquidsoap.info/) installed using [OPAM (OCaml Package Manager)](https://opam.ocaml.org/)
- [Docker](https://www.docker.com/), optional if you want to run in a container
- `git`, `make`

## Installing Engine Core

Install system dependencies:

```bash
apt install curl alsa-utils libasound2-dev libavcodec-dev libavdevice-dev libavformat-dev libavutil-dev libflac-dev libjack-dev libpulse-dev libswresample-dev libswscale-dev libssl-dev ffmpeg opam
```

Build Liquidsoap with additional libraries:

```bash
opam depext alsa pulseaudio bjack ffmpeg samplerate flac taglib mad lame vorbis flac opus cry ocurl liquidsoap -y
opam install alsa pulseaudio bjack ffmpeg samplerate flac taglib mad lame vorbis flac opus cry ocurl liquidsoap -y
```

Now initialize the project environment for development

```bash
make init.dev
```

This command also creates a default configuration file `config/engine-core.ini`.

## Configuring Engine Core

### Configure the audio interface

By default only audio output is enabled using the systems default ALSA device.

$`\textcolor{red}{\text{Ensure PulseAudio server is disabled}}`$

You get the most glitch-free experience when using [ALSA](https://www.alsa-project.org/) devices
directly. To avoid any play-out malfunctions ensure that no PulseAudio server is running.

To check if PulseAudio is started, run `make audio.pa.status`. If this command returns an error,
PulseAudio is deactivated.

If you want to choose a different device, edit the configuration file and set a value for `output_device_0`.

It is also possible to set ALSA hardware device IDs like `hw:0,0`. Check the FAQ below on how to
retrieve available audio device IDs.

$`\textcolor{green}{\text{Recommended audio device configuration}}`$

For better I/O performance it is recommended to create a custom ALSA PCM device named `pcm.aura_engine`, matching your device settings.

You can use the sample configuration [`sample.asoundrc`](https://gitlab.servus.at/aura/engine-core/-/blob/main/config/sample.asoundrc) as a basis for creating such device. Consult the ALSA documentation on details. After creating such device verify its properly
assigned in the configuration file as `output_device_0="pcm.aura_engine"`.

The following command creates a symlink in `config/.asoundrc` to your ALSA config in your home directory:

```bash
make audio.alsa.init
```

## Configure the audio source locations

Engine Core is requires different audio sources in order to perform the playout.

### Configure the location for fallback music

By default fallback audio is retrieved from the `fallback` folder. A local folder for any
emergency playback, also called _Station Fallback_.

```bash
audio/fallback/
```

All audio files inside are played in a randomized order, in situations where nothing is scheduled.
The folder is being watched for changes. So you can add/remove audio on the fly.

This fallback feature is enabled by default, but can be turned off in via the configuration.

Instead of the fallback folder you can use a playlist in the `playlist` folder for fallback
scenarios. Its default file location is:

```bash
audio/playlist/station-fallback-playlist.m3u
```

Also this playlist is being watched for changes. You'll need to set the configuration option
`fallback_type="playlist"` to enable this instead of the fallback folder.

### Configure the audio source folder

This is the location for actually scheduled audio files. They are provided by Tank.

```bash
audio/source
```

If you are running all AURA services on a single instance you should be fine with just creating a
symbolic link to the relevant Tank folder (`ln -s $TANK_STORE_PATH audio/source`). But in some
[distributed and redundant production scenario](https://docs.aura.radio/en/latest/administration/deployment-scenarios.html)
you might think about more advanced options on how to sync your audio files between machines.

You can find some ideas in the doc "[Setting up the Audio Store](https://docs.aura.radio/en/latest/administration/setup-audio-store.html)".

### Configure via environment variables

Many settings of Engine Core can also be set via environment variables. They are primarily used
for Docker deployments. Check the `Makefile` and `sample.env` for hints on environment variables.

## Running Engine Core

To make the playout server play some music first create a folder `audio/fallback/` drop some
music files. This folder is picked up as a so-called _Station Fallback_ in case no other music
is scheduled or if silence is detected.

To start the server execute

```bash
make run
```

Voilá, you should hear some music!

## Advanced tips for development

### Control playout via telnet

Connect to Liquidsoap via [Telnet](https://en.wikipedia.org/wiki/Telnet)

`make tns`

List available commands

`help`

List all available channels

`list`

List all input channels connected to the mixer

`mixer.inputs`

Set the volume of mixer `input 0` to `100%`

`mixer.volume 0 100`

Push some audio file to the filesystem `in_queue_0`

`in_queue_0.push /path/to/your/file.mp3`
