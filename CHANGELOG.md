# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha2] - 2023-06-19

### Changed

- Make properties in API schemas in CamelCase notation (aura#141)
- Configuration: Renamed environment variable `AURA_ENGINE_SERVER_TIMEOUT` to `AURA_ENGINE_SERVER_TIMEOUT`
  and configuration setting `telnet_server_timeout` to `server_timeout`.
- Configuration: Increase default value for `server_timeout`, to avoid malfunctions when idle (aura#165)

### Fixed

- Telnet server sample config not working (engine-core#45).
- Extend and improve output for `make help`.

## [1.0.0-alpha1] - 2023-02-22

Initial release.

