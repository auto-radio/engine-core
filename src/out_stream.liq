#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



# Output streaming settings
# What a mess...
s0_encoding = get_setting("ogg", "stream_0_encoding", "AURA_ENGINE_STREAM_OUTPUT_ENCODING")
s0_bitrate = int_of_string(get_setting("192", "stream_0_bitrate", "AURA_ENGINE_STREAM_OUTPUT_BITRATE"))
s0_host = get_setting("", "stream_0_host", "AURA_ENGINE_STREAM_OUTPUT_HOST")
s0_port = int_of_string(get_setting("", "stream_0_port", "AURA_ENGINE_STREAM_OUTPUT_PORT"))
s0_user = get_setting("", "stream_0_user", "AURA_ENGINE_STREAM_OUTPUT_USER")
s0_pass = get_setting("", "stream_0_password", "AURA_ENGINE_STREAM_OUTPUT_PASSWORD")
s0_mount = get_setting("", "stream_0_mountpoint", "AURA_ENGINE_STREAM_OUTPUT_MOUNTPOINT")
s0_url = get_setting("", "stream_0_url", "AURA_ENGINE_STREAM_OUTPUT_URL")
s0_desc = get_setting("", "stream_0_description", "AURA_ENGINE_STREAM_OUTPUT_DESCRIPTION")
s0_genre = get_setting("", "stream_0_genre", "AURA_ENGINE_STREAM_OUTPUT_GENRE")
s0_name = get_setting("", "stream_0_name", "AURA_ENGINE_STREAM_OUTPUT_NAME")
s0_channels = get_setting("", "stream_0_channels", "AURA_ENGINE_STREAM_OUTPUT_CHANNELS")

s1_encoding = list.assoc(default="", "stream_1_encoding", ini)
s1_bitrate = int_of_string(list.assoc(default="", "stream_1_bitrate", ini))
s1_host = list.assoc(default="", "stream_1_host", ini)
s1_port = int_of_string(list.assoc(default="", "stream_1_port", ini))
s1_user = list.assoc(default="", "stream_1_user", ini)
s1_pass = list.assoc(default="", "stream_1_password", ini)
s1_mount = list.assoc(default="", "stream_1_mountpoint", ini)
s1_url = list.assoc(default="", "stream_1_url", ini)
s1_desc = list.assoc(default="", "stream_1_description", ini)
s1_genre = list.assoc(default="", "stream_1_genre", ini)
s1_name = list.assoc(default="", "stream_1_name", ini)
s1_channels = list.assoc(default="", "stream_1_channels", ini)

s2_encoding = list.assoc(default="", "stream_2_encoding", ini)
s2_bitrate = int_of_string(list.assoc(default="", "stream_2_bitrate", ini))
s2_host = list.assoc(default="", "stream_2_host", ini)
s2_port = int_of_string(list.assoc(default="", "stream_2_port", ini))
s2_user = list.assoc(default="", "stream_2_user", ini)
s2_pass = list.assoc(default="", "stream_2_password", ini)
s2_mount = list.assoc(default="", "stream_2_mountpoint", ini)
s2_url = list.assoc(default="", "stream_2_url", ini)
s2_desc = list.assoc(default="", "stream_2_description", ini)
s2_genre = list.assoc(default="", "stream_2_genre", ini)
s2_name = list.assoc(default="", "stream_2_name", ini)
s2_channels = list.assoc(default="", "stream_2_channels", ini)

s3_encoding = list.assoc(default="", "stream_3_encoding", ini)
s3_bitrate = int_of_string(list.assoc(default="", "stream_3_bitrate", ini))
s3_host = list.assoc(default="", "stream_3_host", ini)
s3_port = int_of_string(list.assoc(default="", "stream_3_port", ini))
s3_user = list.assoc(default="", "stream_3_user", ini)
s3_pass = list.assoc(default="", "stream_3_password", ini)
s3_mount = list.assoc(default="", "stream_3_mountpoint", ini)
s3_url = list.assoc(default="", "stream_3_url", ini)
s3_desc = list.assoc(default="", "stream_3_description", ini)
s3_genre = list.assoc(default="", "stream_3_genre", ini)
s3_name = list.assoc(default="", "stream_3_name", ini)
s3_channels = list.assoc(default="", "stream_3_channels", ini)

s4_encoding = list.assoc(default="", "stream_4_encoding", ini)
s4_bitrate = int_of_string(list.assoc(default="", "stream_4_bitrate", ini))
s4_host = list.assoc(default="", "stream_4_host", ini)
s4_port = int_of_string(list.assoc(default="", "stream_4_port", ini))
s4_user = list.assoc(default="", "stream_4_user", ini)
s4_pass = list.assoc(default="", "stream_4_password", ini)
s4_mount = list.assoc(default="", "stream_4_mountpoint", ini)
s4_url = list.assoc(default="", "stream_4_url", ini)
s4_desc = list.assoc(default="", "stream_4_description", ini)
s4_genre = list.assoc(default="", "stream_4_genre", ini)
s4_name = list.assoc(default="", "stream_4_name", ini)
s4_channels = list.assoc(default="", "stream_4_channels", ini)

s0_connected = ref ('')
s1_connected = ref ('')
s2_connected = ref ('')
s3_connected = ref ('')
s4_connected = ref ('')

if s0_enable == true then
    # enable connection status for that stream
    server.register(namespace="out_http_0", "connected", fun (s) -> begin
        ignore(s)
        !s0_connected
    end)

    # aaand stream
    stream_to_icecast("out_http_0", s0_encoding, s0_bitrate, s0_host, s0_port, s0_pass, s0_mount, s0_url, s0_desc, s0_genre, s0_user, output_source, "0", s0_connected, s0_name, s0_channels)
end

if s1_enable == true then
    server.register(namespace="out_http_1", "connected", fun (s) -> begin
        ignore(s)
        !s1_connected
    end)
    stream_to_icecast("out_http_1", s1_encoding, s1_bitrate, s1_host, s1_port, s1_pass, s1_mount, s1_url, s1_desc, s1_genre, s1_user, output_source, "1", s1_connected, s1_name, s1_channels)
end

if s2_enable == true then
    server.register(namespace="out_http_2", "connected", fun (s) -> begin
        ignore(s)
        !s2_connected
    end)
    stream_to_icecast("out_http_2", s2_encoding, s2_bitrate, s2_host, s2_port, s2_pass, s2_mount, s2_url, s2_desc, s2_genre, s2_user, output_source, "2", s2_connected, s2_name, s2_channels)
end

if s3_enable == true then
    server.register(namespace="out_http_3", "connected", fun (s) -> begin
        ignore(s)
        !s3_connected
    end)
    stream_to_icecast("out_http_3", s3_encoding, s3_bitrate, s3_host, s3_port, s3_pass, s3_mount, s3_url, s3_desc, s3_genre, s3_user, output_source, "3", s3_connected, s3_name, s3_channels)
end

if s4_enable == true then
    server.register(namespace="out_http_4", "connected", fun (s) -> begin
        ignore(s)
        !s4_connected
    end)
    stream_to_icecast("out_http_4", s4_encoding, s4_bitrate, s4_host, s4_port, s4_pass, s4_mount, s4_url, s4_desc, s4_genre, s4_user, output_source, "4", s4_connected, s4_name, s4_channels)
end
