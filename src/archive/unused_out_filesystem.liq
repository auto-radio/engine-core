#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



r0_enable = list.assoc(default="", "rec_0", ini) == "true"
r1_enable = list.assoc(default="", "rec_1", ini) == "true"
r2_enable = list.assoc(default="", "rec_2", ini) == "true"
r3_enable = list.assoc(default="", "rec_3", ini) == "true"
r4_enable = list.assoc(default="", "rec_4", ini) == "true"

r0_folder = list.assoc(default="", "rec_0_folder", ini)
r0_duration = int_of_string(list.assoc(default="", "rec_0_duration", ini))
r0_encoding = list.assoc(default="", "rec_0_encoding", ini)
r0_bitrate = int_of_string(list.assoc(default="", "rec_0_bitrate", ini))
r0_channels = list.assoc(default="", "rec_0_channels", ini)
r0_filenamepattern = r0_folder^"/%Y-%m-%d/%Y-%m-%d-%H-%M."^r0_encoding

r1_folder = list.assoc(default="", "rec_1_folder", ini)
r1_duration = int_of_string(list.assoc(default="", "rec_1_duration", ini))
r1_encoding = list.assoc(default="", "rec_1_encoding", ini)
r1_bitrate = int_of_string(list.assoc(default="", "rec_1_bitrate", ini))
r1_channels = list.assoc(default="", "rec_1_channels", ini)
r1_filenamepattern = r1_folder^"/%Y-%m-%d/%Y-%m-%d-%H-%M."^r1_encoding

r2_folder = list.assoc(default="", "rec_2_folder", ini)
r2_duration = int_of_string(list.assoc(default="", "rec_2_duration", ini))
r2_encoding = list.assoc(default="", "rec_2_encoding", ini)
r2_bitrate = int_of_string(list.assoc(default="", "rec_2_bitrate", ini))
r2_channels = list.assoc(default="", "rec_2_channels", ini)
r2_filenamepattern = r2_folder^"/%Y-%m-%d/%Y-%m-%d-%H-%M."^r2_encoding

r3_folder = list.assoc(default="", "rec_3_folder", ini)
r3_duration = int_of_string(list.assoc(default="", "rec_3_duration", ini))
r3_encoding = list.assoc(default="", "rec_3_encoding", ini)
r3_bitrate = int_of_string(list.assoc(default="", "rec_3_bitrate", ini))
r3_channels = list.assoc(default="", "rec_3_channels", ini)
r3_filenamepattern = r3_folder^"/%Y-%m-%d/%Y-%m-%d-%H-%M."^r3_encoding

r4_folder = list.assoc(default="", "rec_4_folder", ini)
r4_duration = int_of_string(list.assoc(default="", "rec_4_duration", ini))
r4_encoding = list.assoc(default="", "rec_4_encoding", ini)
r4_bitrate = int_of_string(list.assoc(default="", "rec_4_bitrate", ini))
r4_channels = list.assoc(default="", "rec_4_channels", ini)
r4_filenamepattern = r4_folder^"/%Y-%m-%d/%Y-%m-%d-%H-%M."^r4_encoding

r0_is_recording = ref false
r1_is_recording = ref false
r2_is_recording = ref false
r3_is_recording = ref false
r4_is_recording = ref false

if r0_enable == true then
    # enable recording status for that recorder
    server.register(namespace="out_filesystem_0", "recording", fun (s) -> begin if !r0_is_recording == false then "false" else "true" end end)

    # start the recorder
    start_recorder(r0_folder, r0_duration, r0_encoding, r0_bitrate, r0_channels, r0_filenamepattern, r0_is_recording, output_source, "0")
end

if r1_enable == true then
    server.register(namespace="out_filesystem_1", "recording", fun (s) -> begin if !r1_is_recording == false then "false" else "true" end  end)
    start_recorder(r1_folder, r1_duration, r1_encoding, r1_bitrate, r1_channels, r1_filenamepattern, r1_is_recording, output_source, "1")
end

if r2_enable == true then
    server.register(namespace="out_filesystem_2", "recording", fun (s) -> begin if !r2_is_recording == false then "false" else "true" end  end)
    start_recorder(r2_folder, r2_duration, r2_encoding, r2_bitrate, r2_channels, r2_filenamepattern, r2_is_recording, output_source, "2")
end

if r3_enable == true then
    server.register(namespace="out_filesystem_3", "recording", fun (s) -> begin if !r3_is_recording == false then "false" else "true" end  end)
    start_recorder(r3_folder, r3_duration, r3_encoding, r3_bitrate, r3_channels, r3_filenamepattern, r3_is_recording, output_source, "3")
end

if r4_enable == true then
    server.register(namespace="out_filesystem_4", "recording", fun (s) -> begin if !r4_is_recording == false then "false" else "true" end  end)
    start_recorder(r4_folder, r4_duration, r4_encoding, r4_bitrate, r4_channels, r4_filenamepattern, r4_is_recording, output_source, "4")
end