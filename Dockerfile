
FROM savonet/liquidsoap:v2.1.0
LABEL maintainer="David Trattnig <david.trattnig@subsquare.at>"

ENV AURA_UID=2872
ENV AURA_GID=2872

USER root

# Dependencies & Utils
RUN apt update --allow-releaseinfo-change && \
      apt -y --no-install-recommends install \
      build-essential \
      alsa-utils \
      libssl-dev

# Setup Engine
ENV TZ=Europe/Vienna
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir -p /srv/src /srv/tests /srv/config srv/logs srv/socket /var/audio
COPY src /srv/src
COPY tests /srv/tests
COPY config/sample.engine-core.docker.ini /srv/config/engine-core.ini
COPY Makefile /srv/Makefile
COPY VERSION /srv/VERSION

RUN groupadd --gid ${AURA_GID} aura && \
      useradd --gid ${AURA_GID} --no-user-group --uid ${AURA_UID} --home-dir /srv --no-create-home aura && \
      chown -R ${AURA_UID}:${AURA_GID}  /srv /var/audio


USER aura

WORKDIR /srv

# Start the Engine
EXPOSE 1234/tcp
ENTRYPOINT ["make"]
CMD ["run"]