# Docker targets for AURA Makefiles


# Help

define docker_help
	@echo "    docker.build    	- build docker image"
	@echo "    docker.push     	- push docker image"
	@echo "    docker.run      	- start app in container"
	@echo "    docker.run.i    	- start app in container (interactive mode)"
	@echo "    docker.run.bash 	- start bash in container"
	@echo "    docker.run.debug 	- start bash and mount . into container"
	@echo "    docker.restart  	- restart container"
	@echo "    docker.stop     	- stop container"
	@echo "    docker.rm       	- stop and remove container"
	@echo "    docker.log      	- container logs for app"
	@echo "    docker.bash     	- enter bash in running container"
endef

# Dependencies

docker.deps:
	@which docker

# Targets

docker.build:: docker.deps
	@docker build -t autoradio/$(APP_NAME) .

docker.push:: docker.deps
	@docker push autoradio/$(APP_NAME)

docker.run:: DOCKER_ENTRY_POINT := -d
docker.run:: docker.deps
	$(DOCKER_RUN)

docker.run.i:: DOCKER_ENTRY_POINT := -it --rm
docker.run.i:: docker.deps
	$(DOCKER_RUN)

docker.run.bash:: DOCKER_ENTRY_POINT := --entrypoint bash -it --rm
docker.run.bash:: docker.deps
	$(DOCKER_RUN)

docker.run.debug:: DOCKER_ENTRY_POINT := -v "$(CURDIR)":"/srv"  --entrypoint bash -it --rm
docker.run.debug:: docker.deps
	$(DOCKER_RUN)

docker.restart:: docker.deps
	@docker restart $(APP_NAME)

docker.stop:: docker.deps
	@docker stop $(APP_NAME)

docker.rm:: docker.stop
	@docker rm $(APP_NAME)

docker.log:: docker.deps
	@docker logs $(APP_NAME) -f

docker.bash:: docker.deps
	@docker exec -it $(APP_NAME) bash
