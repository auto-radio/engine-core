
#!/usr/bin/env bash
#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


docker run -it -v /dev/snd:/dev/snd -v "/etc/asound.conf":"/etc/asound.conf" --privileged savonet/liquidsoap:main  liquidsoap 'set("log.level",5)' 'set("frame.audio.size", 15052)' 'set("frame.video.framerate", 0)' 'output.alsa(bufferize=false, input.alsa(bufferize=false))'