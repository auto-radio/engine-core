#!/usr/bin/env liquidsoap
#
# Aura Engine (https://gitlab.servus.at/aura/engine)
#
# Copyright (C) 2017-now() - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


 %include "base_config.liq"
settings.log.level.set(3)


##################################################################################################
# Setup sources

# Create fallback playlist
fallback_folder = playlist(id="fallback_folder", "../audio/fallback", mode="randomize")
fallback_folder = mksafe(fallback_folder)

# Event handlers
def on_metadata_notification(meta) =
    print("on_metadata - update (TRACK: #{meta['title']}, ON AIR: #{meta['on_air']}")
end
def on_track_notification(meta) =
    print("on_track    - update (TRACK: #{meta['title']}, ON AIR: #{meta['on_air']}")
end

# Create mixer with one queue
in_queue = request.queue(id="in_queue")
mixer = mix(id="mixer", [ in_queue ])
stripped_mixer = blank.strip(max_blank=5., mixer)

# Output
main_out = fallback(track_sensitive=false, [stripped_mixer, fallback_folder])
main_out.on_metadata(on_metadata_notification)
main_out.on_track(on_track_notification)
output.alsa(device="pcm.aura_engine", mksafe(main_out), bufferize=true)

##################################################################################################
# Test case (Liquidsoap 2.1, DEB or Docker version)

# 1. Add requests
r1 = request.create("assets/audio.mp3")
r2 = request.create("assets/audio.mp3")
r3 = request.create("assets/audio.mp3")
in_queue.push(r1)
in_queue.push(r2)
in_queue.push(r3)

# 2. Fallback is playing
print("Fallback playing")

# 3. Wait 5 seconds and activate queue via server commands
def enable_queue() =
    print("Activate queue")
    r = server.execute("mixer.select 0 true")
    print(r)
    r = server.execute("mixer.volume 0 1")
    print(r)
end
thread.run(delay=5., { enable_queue() })

# 4. Metadata is issued instantly (after the 5 seconds from above):
#
# on_metadata - update (TRACK: Two Pianos), ON AIR: 2022/08/02 12:58:03
# on_track    - update (TRACK: Two Pianos), ON AIR: 2022/08/02 12:58:03

# 5.
#
# FAILURE: When ALSA is used as a PulseAudio Plugin.
# Audio from the queue starts playing only approx. 23 seconds later
# This delay of 23 seconds is always the same, no matter when the server commands
# are issued (e.g. 30secs later) nor how many requests are pushed to the queue
#
# SUCCESS: When ALSA is used directly, ensuring any PulseAudio server is shut down.
# Queued audio is played at the same time, as the metadata from 4.) is issued.
